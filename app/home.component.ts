import { Component } from '@angular/core';
import { Router }           from '@angular/router';
@Component({
  selector: 'my-home',
  template: '<h2>Home Component</h2>'
})
export class HomeComponent {
	  constructor(
    private router: Router) { }
}