import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES }  from '@angular/router';
@Component({
  selector: 'my-app',
  template: `
  <h1>Angular2 routing</h1>
  <nav>
  	<a [routerLink]="['/home']" routerLinkActive="active">Главная</a>
  	<a [routerLink]="['/about']" routerLinkActive="active">О компании</a>
  </nav>
  <router-outlet></router-outlet>
  `,
  directives: [ROUTER_DIRECTIVES]
})
export class AppComponent { }